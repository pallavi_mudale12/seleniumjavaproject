Selenium is a automation testing tool. It is used to automate web based applications. It supports multiple browsers, works on mulitple OS and can be coded in multiple languages.

This project consists of selenium 4.1 version.

Have used 4 interfaces provided by selenium: 
1. WebDriver
2.TakeScreenshot
3. JavaScriptExecutor
4. Wait

It consists of:
1. Direct_locator -> Implements direct locator of selenium
2. Calendar -> Implements calendar logic
3. DropDrown -> Implements dropdrown logic
4. Action -> Implements Action logic
5. ActionDropDown -> Implements action on Drop down.
