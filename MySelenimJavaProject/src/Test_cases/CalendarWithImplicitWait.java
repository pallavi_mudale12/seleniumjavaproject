package Test_cases;

import java.time.Duration;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Common_functions.Handle_browser;

public class CalendarWithImplicitWait {

	public static void main(String[] args) {
		WebDriver Driver = Handle_browser.instantiatebrowser("Chrome");
		Handle_browser.LaunchURL(Driver, "https://www.easemytrip.com/");

		// Implicit wait
		Driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3000));

		// Calendar webElements
		WebElement ShowDate = Driver.findElement(By.xpath("//div[@class=\"date input-box \"]"));
		WebElement DepartDate = Driver.findElement(By.xpath("//input[@placeholder=\"Departure\"]"));
		WebElement ReturnDate = Driver.findElement(By.xpath("//input[@placeholder=\"Return\"]/.."));

		// Current Departure Date
	DepartDate.click();

		WebElement CurrentDate = Driver.findElement(By.xpath("//li[@class=\"active-date\"]"));
		CurrentDate.click();

		// Any and Return Date
		ReturnDate.click();

		List<WebElement> Date_Click = Driver.findElements(
				By.xpath("//div[text()=\"Feb 2024\"]/../..//li[@class=\"\" and @onclick=\"SelectDate(this.id)\"]"));

		int count = Date_Click.size();

		for (int i = 0; i < count; i++) {

		String date_value = Date_Click.get(i).getText();
			System.out.println(date_value);

			if (date_value.contains("19")) {
				Date_Click.get(i).click();
			break;
			} else {
				System.out.println("Desired date is not found in current iteration : " + i + " , Hence retrying");
			}
	}

		Driver.quit();

	}

}
