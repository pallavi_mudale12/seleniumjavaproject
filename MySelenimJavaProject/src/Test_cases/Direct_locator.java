package Test_cases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import Common_functions.Handle_browser;

public class Direct_locator {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver = Handle_browser.instantiatebrowser("Chrome");
		driver.get("https://www.amazon.in/");
		driver.manage().window().maximize();
//	    driver.findElement(By.id("nav-logo-sprites")).click();
//      driver.quit();
//	    Thread.sleep(5000);
//		driver.findElement(By.className("nav-progressive-content")).click();
//		driver.findElement(By.linkText("Best Sellers")).click();
		driver.findElement(By.partialLinkText("miniTV")).click();
		driver.quit();
	}

}
