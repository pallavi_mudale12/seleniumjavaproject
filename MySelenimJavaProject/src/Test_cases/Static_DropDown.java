package Test_cases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import Common_functions.Handle_browser;

public class Static_DropDown {

	public static void main(String[] args) {
		WebDriver Driver = Handle_browser.instantiatebrowser("Chrome");
		Handle_browser.LaunchURL(Driver, "https://www.amazon.in/");

		// Find the dropdown webelement using the xpath of dropdown to be automated
		WebElement dropdown = Driver.findElement(By.xpath("//select[@title=\"Search in\"]"));
		List<WebElement> options = Driver.findElements(By.xpath("//select[@title=\"Search in\"]/option"));

		// Create and object of select class with argument of dropdown webelement.
		Select staticdropdown = new Select(dropdown);

		// Select by index
		// staticdropdown.selectByIndex(6);

		// Select by value
		// staticdropdown.selectByValue("search-alias=audible");

		// Select by visible text
		// staticdropdown.selectByVisibleText("Luggage & Bags");

		// Multi select
		int count = options.size();
		for (int i = 0; i < count; i++) {
			staticdropdown.selectByIndex(i);
		}

		Driver.quit();

	}

}
