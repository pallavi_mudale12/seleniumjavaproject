package Test_cases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Common_functions.Handle_browser;

public class AutoSuggestiveDynamicDropDown {

	public static void main(String[] args) throws InterruptedException {
		WebDriver Driver = Handle_browser.instantiatebrowser("Chrome");
		Handle_browser.LaunchURL(Driver, "https://www.google.co.in/");

		// Fetch the webelement of dropdown
		WebElement searchbox = Driver.findElement(By.xpath("//textarea[@aria-label=\"Search\"]"));

		// Insert text
		searchbox.sendKeys("selenium");

		// Wait for 3 seconds so that list is visible on screen
		Thread.sleep(3000);

		// Fetch the options webelement
		List<WebElement> options_click = Driver.findElements(By.xpath("(//ul[@role=\"listbox\"])[1]/li"));
		List<WebElement> options_text = Driver
				.findElements(By.xpath("(//ul[@role=\"listbox\"])[1]/li/div/div[2]/div[1]/div[1]/span/b"));

		// Fetch the count of options webelements
		int count = options_click.size();

		for (int i = 0; i < count - 1; i++) {
			String option_value = options_text.get(i).getText();
			System.out.println(option_value);
			if (option_value.equals("dev")) {
				options_click.get(i + 1).click();
				break;
			} else {
				System.out.println("Desired option not found after checking " + i + "th option");
			}

		}

		Driver.quit();

	}

}
