package Test_cases;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Common_functions.Handle_browser;

public class Calendar {

	public static void main(String[] args) {
		WebDriver Driver = Handle_browser.instantiatebrowser("Chrome");
		Handle_browser.LaunchURL(Driver, "https://www.redbus.com/");

		// Implicit wait
		Driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3000));
		
		WebElement cookie = Driver.findElement(By.xpath("//*[@id='cookie-root']/div/div/div/button"));
		cookie.click();

		// Calendar webElements
		WebElement DepartDate = Driver.findElement(By.xpath("//input[@placeholder='DD/MY']"));
		WebElement ReturnDate = Driver.findElement(By.xpath("//input[@placeholder=\"Optional\"]/.."));

		// Current Departure Date
		DepartDate.click();
		
		WebElement CurrentDate = Driver.findElement(By.xpath("//span[text()='21']"));
		
		
		
		//Actions actions = new Actions(Driver);

		//actions.moveToElement(CurrentDate).click().perform();

		CurrentDate.click();

	}

}
