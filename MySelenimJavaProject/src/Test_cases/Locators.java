package Test_cases;

import org.openqa.selenium.WebDriver;

import Common_functions.Handle_browser;

public class Locators {

	public static void main(String[] args) {
		WebDriver chrome_driver = Handle_browser.instantiatebrowser("Chrome");
		chrome_driver.get("https://www.facebook.com/");
		chrome_driver.quit();
		WebDriver firefox_driver = Handle_browser.instantiatebrowser("Firefox");
		firefox_driver.get("https://www.facebook.com/");
		firefox_driver.quit();
		WebDriver edge_driver = Handle_browser.instantiatebrowser("Edge");
		edge_driver.get("https://www.facebook.com/");
		edge_driver.quit();

	}

}
