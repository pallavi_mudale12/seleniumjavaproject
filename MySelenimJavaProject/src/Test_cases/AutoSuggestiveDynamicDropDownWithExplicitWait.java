package Test_cases;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Common_functions.Handle_browser;

public class AutoSuggestiveDynamicDropDownWithExplicitWait {

	public static void main(String[] args) {
		WebDriver Driver =Handle_browser.instantiatebrowser("Chrome");
		Handle_browser.LaunchURL(Driver, "https://www.google.co.in/");
		 //Create an object of WebDriverWait (explicit wait)
		 WebDriverWait explicit_Wait = new WebDriverWait(Driver, Duration.ofSeconds(5));
		 
		 //Fetch the webelement of dropdown
		 WebElement searchbox = explicit_Wait.until
				 (ExpectedConditions.elementToBeClickable
						 (By.xpath("//textarea[@aria-label=\"Search\"]")));
		 
		 //Insert text
		 searchbox.sendKeys("selenium");
		 
		 //Fetch the options webelement
		 List<WebElement> options_click = explicit_Wait.until
				 (ExpectedConditions.visibilityOfAllElementsLocatedBy
						 (By.xpath("(//ul[@role=\"listbox\"])[1]/li")));
		 
		 List<WebElement> options_text = explicit_Wait.until
				 (ExpectedConditions.visibilityOfAllElementsLocatedBy
						 (By.xpath("(//ul[@role=\"listbox\"])[1]/li/div/div[2]/div[1]/div[1]/span/b")));
		 
		 
		 //Fetch the count of options webelements
		 int count=options_click.size();
		 
		 for(int i=0 ; i<count-1 ; i++)
		 {
			 String option_value = options_text.get(i).getText();
			 System.out.println(option_value);
			 if(option_value.equals("dev")) {
				 options_click.get(i+1).click();
				 break;
			 }
			 else {
				 System.out.println("Desired option not found after checking "+i+ "th option");
			 }
			 
		 }
		 
		 Driver.quit();
				 
		

	}

}
