package Instantiate_browser_reference;

import org.openqa.selenium.chrome.ChromeDriver;

public class Instantiate_browser_system_property {

	public static void main(String[] args) {
		//step 1 set the system property to locate the web driver binary file
		//System.setProperty("webdriver.com.driver", "C:\\Users\\HP\\eclipse-workspace\\MySeleniumJavaProject\\chromedriver-win64\\chromedriver.exe\\");
		//step 2 create the reference variable of chrome driver
				//WebDriver driver=new ChromeDriver();
				//step 2 create the object of chrome driver
						ChromeDriver driver=new ChromeDriver();
				//step 3 Launch the URL
				driver.get("https://www.instagram.com/");
	
	}

}
