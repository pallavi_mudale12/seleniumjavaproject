
package Common_functions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Handle_browser {
	public static WebDriver instantiatebrowser(String Browser) {
		WebDriver driver;
		switch (Browser) {
		case "Chrome":
			driver = new ChromeDriver();
			break;
		case "Firefox":
			driver = new FirefoxDriver();
			break;
		case "Edge":
			driver = new EdgeDriver();
			break;
		default:
			driver = new ChromeDriver();
		}
		return driver;
    }
	public static void LaunchURL(WebDriver Driver, String URL) {
		Driver.get(URL);
		Driver.manage().window().maximize();
	}
}
