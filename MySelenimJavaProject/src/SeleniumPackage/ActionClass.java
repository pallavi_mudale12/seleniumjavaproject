package SeleniumPackage;

import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import Common_functions.Handle_browser;

public class ActionClass {

	public static void main(String[] args) {
		WebDriver Driver = Handle_browser.instantiatebrowser("Chrome");
		Handle_browser.LaunchURL(Driver, "https://www.amazon.in/");
		WebDriverWait exp_wait = new WebDriverWait(Driver, Duration.ofSeconds(5));

		// Create the object of Actions class
		Actions act = new Actions(Driver);

		// Double Click
		WebElement amazonLogo = exp_wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@aria-label=\"Amazon.in\"]")));

		amazonLogo.click();

		WebElement DoubleclickArrow = exp_wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()=\"Next slide\"]/..")));

		act.moveToElement(DoubleclickArrow).doubleClick().build().perform();

		// Insert Text In Upper Case
		// WebElement of search box
		WebElement searchBox = exp_wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@aria-label=\"Search Amazon.in\"]")));

		act.moveToElement(searchBox).click().keyDown(Keys.SHIFT).sendKeys("Iphone").keyUp(Keys.SHIFT)
				.keyDown(Keys.ENTER).build().perform();

		/*
		 * Step 1 : Move your cursor to the search box : act.moveToElement(searchBox)
		 * 
		 * Step 2 : Click the search box : .click()
		 * 
		 * Step 3 : Press shift button on keyboard : .keyDown(Keys.SHIFT)
		 * 
		 * Step 4 : Type the text you want to search : .sendKeys("Iphone")
		 * 
		 * Step 5 : Release shift button : .keyUp(Keys.SHIFT)
		 * 
		 * Step 6 : Press enter button on keyboard : .keyDown(Keys.ENTER)
		 */

		// Right Click

		WebElement hellSignIn = exp_wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()=\"Hello, sign in\"]")));

		act.moveToElement(hellSignIn).contextClick().build().perform();

		Driver.quit();
	}

}
