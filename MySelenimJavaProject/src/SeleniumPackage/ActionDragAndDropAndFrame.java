package SeleniumPackage;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Common_functions.Handle_browser;

public class ActionDragAndDropAndFrame {

	public static void main(String[] args) {
		WebDriver Driver = Handle_browser.instantiatebrowser("Chrome");
		Handle_browser.LaunchURL(Driver, "https://jqueryui.com/droppable/");
		WebDriverWait exp_wait = new WebDriverWait(Driver, Duration.ofSeconds(5));

		// Create the object of Actions class
		Actions act = new Actions(Driver);

		// Switch inside the frame
		WebElement frame = exp_wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//iframe[@class=\"demo-frame\"]")));
		Driver.switchTo().frame(frame);

		// Drag and Drop
		WebElement source_element = exp_wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id=\"draggable\"]")));

		WebElement target_element = exp_wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id=\"droppable\"]")));

		act.dragAndDrop(source_element, target_element).build().perform();

		// Come out of the frame to parent frame
		Driver.switchTo().parentFrame();

		WebElement parent_frame_element = exp_wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()=\"Resizable\"]")));

		parent_frame_element.click();

		Driver.quit();

	}

}
